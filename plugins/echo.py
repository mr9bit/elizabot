from handler.base_plugin import BasePlugin
import pandas as pd
import vk_api
from sklearn.feature_extraction.text import TfidfVectorizer
import re
import string
from sklearn.metrics.pairwise import linear_kernel


def find_similar(vector, tfidf_matrix, top_n=15):
    cosine_similarities = linear_kernel(vector, tfidf_matrix).flatten()
    related_docs_indices = [i for i in cosine_similarities.argsort()[::-1]]
    return [(index, cosine_similarities[index]) for index in related_docs_indices][0:top_n]

def preprocessing(line):
    line = line.lower()
    line = re.sub(r"[{}]".format(string.punctuation), " ", line)
    return line

class EchoPlugin(BasePlugin):
    __slots__ = ()

    def __init__(self):
        """Answers with a message it received."""
        self.df = pd.read_csv('plugins/maks_clean.csv')
        self.corpus = []
        for idex, item in self.df.iterrows():
            self.corpus.append((item['Компания'], item['Описание'], item['Категория']))
        self.tf =  TfidfVectorizer(ngram_range=(1, 4), preprocessor=preprocessing)
        self.matrix = self.tf.fit_transform([content for name, content, category in self.corpus])
        vk_session = vk_api.VkApi('89109143819', '256128qwe', api_version="5.101")
        vk_session.auth()
        self.vk = vk_session.get_api()

        super(self.__class__, self).__init__()


    async def check_message(self, msg):
        return True

    async def process_message(self, msg):
        user_vk = self.vk.users.get(user_ids=msg.user_id)
        user_id = int(user_vk[0]['id'])
        groups = self.vk.groups.get(user_id=user_id, fields=['description'], extended=True)
        lists = ''
        for item in groups['items']:
            lists += f"{item['name']}"
        vec = self.tf.transform([lists])
        company_list = []
        for index, score in find_similar(vec, self.matrix):
            company_list.append({'name': self.corpus[index][0], 'desc': self.corpus[index][1], 'category': self.corpus[index][2]})
        category_count = {}
        for item in company_list:
            category_count[item['category']] = 0
        for item in company_list:
            category_count[item['category']] += 1
        sort_category = sorted(category_count.items(), key=lambda x: x[1], reverse=True)[:2]
        answer = 'Смотри, что удалось подобрать специально для тебя\n Все компании представленные на МАКС 2019, и ты найдешь их в буклетах'
        answer += '\n\n'
        answer += 'Вам больше всего нравится:\n'
        for cat in sort_category:
            answer += f'- {cat[0]}\n'
        answer += '\n\nСписок рекомендаций к посещению\n'
        for company in company_list:
            answer += f"- {company['name']}\n{company['desc']}\n\n"
        await msg.answer(answer)
